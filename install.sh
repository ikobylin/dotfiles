#!/usr/bin/env bash

DIR_CURRENT=$(dirname $0)
DIR_BACKUP="$DIR_CURRENT/backup"
DIR_DOTFILES=$(cd "$DIR_CURRENT"; pwd)

cd "${DIR_CURRENT}" || exit 1
mkdir -p "${DIR_BACKUP}"
mkdir -p "${HOME}/.config"

install() {
    local dst=$1
    local src=$2
    test -e "${HOME}/${dst}" && mv "${HOME}/${dst}" "${DIR_BACKUP}/" 
    test -r "${DIR_CURRENT}/${src}" && ln -s "${DIR_DOTFILES}/${src}" "${HOME}/${dst}"
}

install .bashrc bashrc
install .bash_profile bash_profile
install .profile profile

cd $OLDPWD
