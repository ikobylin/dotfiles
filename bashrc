# Check for an interactive session
[ -z "$PS1" ] && return


# Global vairables
export PAGER=less
export GREP_OPTIONS='--color=auto'
export LS_OPTIONS='--color=always'
export GN_EDITOR=nano


# PATH
PATH="/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin"
test -e "${HOME}/bin" && PATH="${HOME}/bin:${PATH}"
test -e "${HOME}/Dev/browser/depot_tools" && PATH="${HOME}/Dev/browser/depot_tools:${PATH}"


# Prompt
console_code() {
    local codes=( $@ )
    local code=$(printf ";%s" "${codes[@]}")
    code=${code:1}
    echo "\[\e[${code}m\]"
}

prompt_command() {
    local RETURN_CODE="$?"

    local RESET=`console_code 0`
    local BOLD=1

    local FG_BLACK=30
    local FG_RED=31
    local FG_GREEN=32
    local FG_BROWN=33
    local FG_BLUE=34
    local FG_MAGENTA=35
    local FG_CYAN=36
    local FG_WHITE=37
    local BG_BLACK=40
    local BG_RED=41
    local BG_GREEN=42
    local BG_BROWN=43
    local BG_BLUE=44
    local BG_MAGENTA=45
    local BG_CYAN=46
    local BG_WHITE=47

    local USER_FMT=`console_code 0 $BOLD $FG_GREEN`
    local HOST_DELIMIT_FMT=`console_code 0 $BOLD $FG_BROWN`
    local HOST_FMT=`console_code 0 $BOLD $FG_BROWN`
    local CWD_DELIMIT_FMT=`console_code 0 $BOLD $FG_BROWN`
    local CWD_FMT=`console_code 0 $BOLD $FG_BLUE`

    local PROMPT_FMT=`console_code 0 $BOLD $FG_WHITE`
    local PROMPT_STRING="\$"
    if (( $EUID == 0 )) ; then
        PROMPT_FMT=`console_code 0 $BOLD $FG_RED`
        PROMPT_STRING="#"
    fi

    local GIT_DELIMIT_FMT=`console_code 0 $BOLD $FG_CYAN`
    local GIT_FMT="$RESET"
    local GIT_CURRENT_BRANCH_NAME=`git rev-parse --abbrev-ref HEAD 2>/dev/null`
    if [[ $GIT_CURRENT_BRANCH_NAME != "" ]] ; then
        GIT_FMT="$GIT_DELIMIT_FMT#$GIT_CURRENT_BRANCH_NAME"
    fi

    local VENV_DELIMIT_FMT=`console_code 0 $FG_BROWN`
    local VENV_FMT="$RESET"
    if [[ -n "$VIRTUAL_ENV" ]] ; then
        VENV_NAME=`basename "$VIRTUAL_ENV"`
        VENV_FMT="$VENV_DELIMIT_FMT;$VENV_NAME"
    fi

    PS1="\n$USER_FMT\u$HOST_DELIMIT_FMT@\h$CWD_DELIMIT_FMT:$CWD_FMT\W$GIT_FMT$VENV_FMT$PROMPT_FMT$PROMPT_STRING$RESET "
}

export PROMPT_COMMAND=prompt_command


# Aliases
## Listings
alias l='ls -hl'
alias lt='l -Gt | tail -20'
if which tree >/dev/null 2>&1 ; then
    alias ll='tree --dirsfirst -ChFupDaL 1'
    alias lll='tree --dirsfirst -ChFupDaL 2'
else
    alias ll='l -A'
fi

## Aptitude
if which aptitude >/dev/null 2>&1 ; then
    alias au='sudo aptitude update'
    alias ai='sudo aptitude install'
    alias asu='sudo aptitude safe-upgrade'
    alias adu='sudo aptitude dist-upgrade'
    alias afu='sudo aptitude full-upgrade'
    alias asr='aptitude search'
fi

## Git
if which git >/dev/null 2>&1 ; then
    alias g='git'
    alias ga='g add'
    alias gb='g branch'
    alias gc='g commit'
    alias gca='gc --amend'
    alias gd='g diff'
    alias gst='g stash'
    alias go='g checkout'
    alias gp='git pull'
    alias gpr='gp --rebase'
    alias gt='g status'
    alias gu='g reset HEAD $(g rev-parse --show-cdup)'
fi

## Misc
alias cl='clear'
alias se='sudo nano'


# Local settings
[ -r "${HOME}/.local/bashrc" ] && source "${HOME}/.local/bashrc"
